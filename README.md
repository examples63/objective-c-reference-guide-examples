# Objective-C 2.0 reference guide examples


[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Tested on **Xcode 14.1**

## Table of contents

1. [Comments, console output, Data Types](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L30)
2. [Bool, Integer, Float, Double, Operators](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L61)
3. [if ... else, Conditional (ternary) operator, Logical operators, Assignment Operators](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L119)
4. [For loop, break, continue, While, Do-While](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L162)
5. [Pointers](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L194)
6. [NSString](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L210)
7. [NSArray, NSMutableArray, Fast Enumeration (Objective-C 2.0)](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L235)
8. [Enum, Switch](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L274)
9. [Struct](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L299)
10. [Object init and method calls](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L315)
11. [Introspection](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L370)
12. [Blocks](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L397)
13. [Exception Handling](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L420)
14. [Threads, @synchronized lock](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Other/AppDelegate.m#L448)
15. [Header files, propertires, constructors, methods](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Animal.h#L13)
16. [Classes implementation, methods implementation](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Animal.m#L12)
17. [Protocol](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Koala.h#L12)
18. [Inheritance header](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Koala.h#L37)
19. [Inheritance implementation](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Koala.m#L12)
20. [Category header](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Animal+Exam.h#L11)
21. [Category implementation](https://gitlab.com/examples63/objective-c-reference-guide-examples/-/blob/main/Objective-C%20reference%20guide%20examples/Examples/Animal+Exam.m#L11)
