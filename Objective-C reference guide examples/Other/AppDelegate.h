//
//  AppDelegate.h
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentCloudKitContainer *persistentContainer;

- (void)saveContext;

@end

