//
//  AppDelegate.m
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#import "AppDelegate.h"
#import "Animal.h"
#import "Koala.h"
#import "Animal+Exam.h"
#import <objc/runtime.h>

@interface AppDelegate ()

@end


    // static variable

static float piVal = 3.14;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    // Single line comment
    
    /*
     Multi line
     comment
     */
    
    
    // console output:
    printf("%s", [@"Hello world! \n" UTF8String]);
    NSLog(@"%@", @"Hello world 2!");
    
    
    // Data Types:
    
    // BOOL         = boolean (YES/NO)
    // char         = C char
    // double         = C double
    // float         = C float
    // ClassName *    = new object(class) pointer: NSString *, NSArray *, etc.
    // id             = pointer to an object
    // int         = C int
    // long         = C long
    // short         = C short
    // signed         = C signed
    // unsigned     = C unsigned
    // void         = no type (nil, NULL)
    // self         = refers to the current object in a method
    // super         = refers to the superclass in a method
    
    
    // Bool
    
    bool cBool = true;
    BOOL myBool = YES;
    
    NSLog(@"cBool: %d myBool: %d", cBool, myBool);
    cBool = false;
    myBool = NO;
    
    cBool = 25;
    myBool = 25;
    NSLog(@"cBool: %d myBool: %d", cBool, myBool);
    
    
    // Integer and type casting
    
    NSInteger myInt = 34;
    NSLog(@"Value: %ld",(long)myInt);
    
    myInt++;
    --myInt;
    
    // Float and Double
    
    float f = M_PI;
    double d = M_PI;
    CGFloat cqf = M_PI;
    
    float initial = 0.1;
    f = initial / 3;
    f += initial / 3;
    f += initial / 3;
    
    NSLog(@"Float: %0.12f (%lu bytes)", f, sizeof(f));
    
    printf ("Min Float : %e\n", FLT_MIN);
    printf ( "Max Float: %e\n", FLT_MAX);
    
    
    // Operators
    
    // Arithmetic Operators: + - * / % ++ --
    
    // Relational Operators: == != < > >= <=
    
    // Logical Operators:  &&  ||  !
    
    // Assignment Operators: =  +=  -=  *=  /=  %=
    
    // Misc Operators:
    
    //  ? :     Conditional (ternary) Expression.
    
    //  &       Returns the address of an variable.
    
    //  *       Pointer to a variable.
    
    
    // if ... else
    
    int age = 13;
    
    if ( age <= 6){
        printf ("You're in Kindergarten\n");
    } else if (age <= 13) {
        printf("You're in Elementary\n");
    } else {
        printf("You're in High School\n") ;
    }
    
    
    // Logical operators
    
    // OR
    if ((age >= 12) || (age <= 13)){
        printf( "You're in Elementary") ;
    }
    
    // AND
    bool isElementary = ( age >= 12) && (age <= 13);
    
    //  !  (not)
    printf( "Opposite of True : %d\n", (!true));
    
    
    // Conditional (ternary) operator
    
    bool isHighSchool = (age > 13) ? 1 : 0 ;
    
    
    // Assignment Operators
    
    int z = 0;
    printf("z += 5 %d\n", z += 5);
    
    z *= 5;
    z -= 5;
    z /= 5;
    z %= 5;
    
    
    // For loop, break, continue
    
    for (int i = 1; i <= 10; i++) {
        // jumps out of the for loop
        if (i == 9){
            break;
        }
        
        // continues with next iteration of for loop
        if ( i == 7 ){
            continue;
        }
        
        printf("i :%d\n", i);
    }
    
    
    // While, Do-While
    
    int j = 1;
    
    while (j <= 10) {
        printf("j : %d\n", j);
        j++;
    }
    
    do {
        printf("j : %d\n", j);
        j--;
    } while (j > 0);
    
    
    // Pointers
    
    int randNum = 12345;
    
    // pointer to randNum :
    printf("randNum location : %p\n", &randNum);
    
    // new number has the same address
    int *addrRandNum = &randNum;
    // new value for both
    *addrRandNum = 54321;
    
    printf("randNum value : %d\n", randNum);
    printf("randNum Memory Location : %p\n", &randNum);
    
    
    // NSString
    
    NSString *personOne = @"Ray";
    NSString *personTwo = @"Shawn";
    NSString *combinedString =
    [NSString stringWithFormat: @"%@: Hello, %@!", personOne, personTwo];
    
    NSLog(@"%@", combinedString);
    
    NSString *tipString = @"24.99";
    float tipFloat = [tipString floatValue];
    
    NSLog(@"Size of String : %d", (int) [combinedString length]) ;
    
    BOOL isStringEqual = [personOne isEqualToString: @"Ray"];
    
    NSRange searchResult = [combinedString rangeOfString: @"Shawn"];
    
    if (searchResult. location == NSNotFound){
        NSLog (@"String not found");
    } else {
        NSLog (@"String found");
    }
    
    
    // NSArray
    
    NSArray *officeSupplies = @[@"Pencils", @"Paper"];
    
    NSLog (@"First : %@", officeSupplies[0]);
    
    BOOL containsItem = [officeSupplies containsObject:@"Pencils"];
    
    NSLog (@"Total : %d", (int) [officeSupplies count]);
    
    NSLog (@"Index of Pencils is at %lu", (unsigned long) [officeSupplies indexOfObject: @"Pencils"]);
    
    
    // NSMutableArray
    
    NSMutableArray *array =
    [@[personOne, personTwo] mutableCopy];
    
    [array addObject:@"Waldo"];
    NSLog(@"%lu items!", (unsigned long)[array count]);
    
    [array insertObject:@"Superman" atIndex:2];
    
    [array removeObjectIdenticalTo: @"Superman" inRange:NSMakeRange(0,2)];
    
    [array removeObject:@"Waldo"];
    
    [array removeObjectAtIndex:0];
    
    // Fast Enumeration (Objective-C 2.0)
    
    // for (type variable in expression) { }
    
    for (NSString *person in array) {
        NSLog(@"Person: %@", person);
    }
    
    NSString *somePerson = array[0];
    
    
    // Enum
    
    typedef NS_ENUM(int, VideoGameType){
        VideoGameTypeRPG,
        VideoGameTypeStrategy,
        VideoGameTypeAction,
        VideoGameTypeFPS = 5
    };
    
    VideoGameType type = VideoGameTypeFPS;
    NSLog (@"Sizeof type: %lu Value: %lld", sizeof (type), (long long) type);
    
    
    // Switch
    
    switch (type) {
        case VideoGameTypeRPG:
            NSLog(@"RPG");
            break;
        default:
            NSLog(@"Default");
            break;
    }
    
    
    // Struct
    
    struct Superhero{
        char *realName;
        char *superName;
        float height;
    };
    
    struct Superhero superman;
    
    superman.realName = "Clark Kent";
    superman.superName = "Superman";
    superman.height = 6.25;
    
    printf("%s is the hero named %s. He is %.2f ft tall \n", superman.realName, superman.superName, superman.height);
    
    //    Initializing objects + ARC:
    //
    //    alloc  =  allocate object memory
    //    init   =  initialize object instance
    //    retain =  increment object’s reference count
    //    release = decrement object’s reference count
    
    
    // #import "Animal.h"
    
    Animal *cat = [[Animal alloc]initWithName:@"Whiskers"];
    
    [cat setFavFood:@"mouse"];
    cat.sound = @"Miau";
    
    NSString *catFavFood = [cat favFood];
    NSString *catSound = cat.sound;
    
    NSLog (@"The cats name is %@", cat.name);
    
    NSLog (@"3 + 5 = %d", [cat getSum:3 nextNumber:5]);
    
    NSLog (@"%@", [cat talkToMe: @"Martin"]) ;
    
    // anonymous arguments:
    
    NSLog (@"10 + 5 -3 = %d", [cat anonArgs:10 :5 :3] );
    
    // static class method:
    
    NSLog (@"%@", [Animal processStrings:@"John" second:@"Doe"] );
    
    
    // Inheritence (Koala)
    
    // #import "Koala.h"
    
    Koala *koala = [[Koala alloc]initWithName:@"Becky"];
    
    NSLog (@"%@", [koala talkToMe: @"Martin"]);
    
    [koala performTrick];
    
    [koala makeSound];
    
    
    // Category
    
    // #import "Animal+Exam.h"
    
    if ([cat checkedByVet]){
        [cat getShots];
    }
    
    
    // Introspection (classes, selectors = methods)
    
    // #import <objc/runtime.h>
    
    Class superclass = class_getSuperclass([koala class]);
    
    NSLog (@"Superclass of %@ is %@", NSStringFromClass([koala class]), NSStringFromClass(superclass));
    
    NSLog (@"Koala is Member of NSObject: %d", [koala isMemberOfClass: [NSObject class]]);
    NSLog (@"Koala is Kind of NSObject: %d", [koala isKindOfClass: [NSObject class]]);
    
    NSLog (@"Koala is Member of Animal: %d", [koala isMemberOfClass: [Animal class]]);
    NSLog (@"Koala is Kind of Animal: %d", [koala isKindOfClass: [Animal class]]);
    
    if ([koala respondsToSelector:@selector(makeSound)]){
        [koala performSelector:@selector(makeSound)];
    }
    
    SEL selector = @selector(talkToMe:);
    NSLog(@"Selector: %@", NSStringFromSelector(selector));
    
    Method method = class_getInstanceMethod([koala class], selector);
    
    unsigned int numOfArgs = method_getNumberOfArguments(method);
    NSLog (@"%d arguments", numOfArgs);
    
    
    // Blocks
    
    // declaration
    // returntype (^blockName)(argumentType);
    
    // implementation
    // returntype (^blockName)(argumentType)= ^{ };
    
    void (^simpleBlock)(void) = ^{
        NSLog(@"This is a block");
    };
    
    simpleBlock();
    
    double (^multiplyTwoValues)(double, double) =
    ^(double firstValue, double secondValue) {
        return firstValue * secondValue;
    };
    
    double result = multiplyTwoValues(2,4);
    NSLog(@"The result is %f", result);
    
    
    // Exception Handling
    
    /*
     @try
     {
     }
     @catch (myCustomException) {
     }
     @catch (NSException *ne) { // less specific type
     }
     @finally {  // optional block
     }
     @throw(exception);  // throw an exception
     @throw();  // re-throw current exception
     */
    
    void (^throwException)(void) = ^{
        @throw [NSException exceptionWithName:@"Some exception" reason:@"We want to throw" userInfo:nil];
    };
    
    @try {
        throwException();
    }
    @catch (NSException *ne) { // less specific type
        NSLog(@"Exception: %@, reason: %@", ne.name, ne.reason);
    }
    
    
    // Threads
    
    // asynchronously on main thread
    dispatch_async(dispatch_get_main_queue(),^{
        NSLog(@"asynchronously on main thread");
    });
    
    // asynchronously on background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSLog(@"asynchronously on background thread");
    });
    
    // asynchronously on background thread with return to main thread
    // Always update UI on main thread !
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"asynchronously on background thread");
        dispatch_async(dispatch_get_main_queue(), ^{
            // Always update UI on main thread !
            NSLog(@"Update UI asynchronously on main thread");
        });
    });
    
    
    // Using performSelector
    
    // synchronously on main thread
    [koala performSelectorOnMainThread:@selector(makeSound) withObject:nil waitUntilDone:true];
    
    // asynchronously on main thread
    [koala performSelectorOnMainThread:@selector(makeSound) withObject:nil waitUntilDone:false];
    
    // asynchronously on background thread
    [koala performSelectorInBackground:@selector(makeSound) withObject:nil];
    
    
    // NSThread
    
    [NSThread detachNewThreadSelector:@selector(makeSound) toTarget:koala withObject:nil];
    
    // or:
    NSThread* evtThread = [ [NSThread alloc] initWithTarget:koala
                                                   selector:@selector(makeSound)
                                                     object:nil];
    
    [ evtThread start ];
    
    
    // @synchronized
    
    // @synchronized is a function that takes an object and a closure, and invokes the closure with the lock held:
    
    @synchronized(array) {
        [array addObject: @"Polo"];
    }
    
    // is the same as :
    
    NSLock *aLock;
    
    [aLock lock];
    [array addObject: @"Polo"];
    [aLock unlock];
    
    NSLog(@"%@", [array description]);
    
    return YES;
}

#pragma mark - UISceneSession lifecycle

- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentCloudKitContainer *)persistentContainer {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentCloudKitContainer alloc] initWithName:@"Objective_C_reference_guide_examples"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
