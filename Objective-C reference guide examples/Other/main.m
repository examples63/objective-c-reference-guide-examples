//
//  main.m
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Animal.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
