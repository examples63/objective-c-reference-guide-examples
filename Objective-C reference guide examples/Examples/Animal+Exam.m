//
//  Animal+Animal_Exam.m
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 08/12/2022.
//

#import "Animal+Exam.h"


    // ClassToExtend+CategoryName.m

/*
#import "ClassToExtend+CategoryName.h"

@implementation ClassToExtend (CategoryName)

// implement new methods

@end
*/

@implementation Animal (Exam)

- (BOOL) checkedByVet{
    return 1;
}

- (void) getShots{
    NSLog(@"%@ got its shots", self.name);
}

@end
