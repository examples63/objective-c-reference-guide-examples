//
//  Animal+Animal_Exam.h
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 08/12/2022.
//

#import "Animal.h"


    // Category

//  Categories add new methods to a class - without subclassing
//  Caveat: You cannot add data to them

    // ClassToExtend+CategoryName.h

/*
#import "ClassToExtend.h"

@interface ClassToExtend (CategoryName)

// define new methods

@end
*/

@interface Animal (Exam)

- (BOOL) checkedByVet;
- (void) getShots;

@end


