//
//  Koala.h
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 08/12/2022.
//

#ifndef Koala_h
#define Koala_h


// Protocol

/*
@protocol ProtocolName<ProtocolToExtend>
@optional
// Optional methods
@required
// Required methods
@end

@protocol protocolname <protocol list>
   method declarations
@end

id <ProtocolName> myVariable;
*/

@protocol BeautyContest<NSObject>

- (void) performTrick;
- (void) makeSound;

@end


    // Inheritance

#import "Animal.h"

@interface Koala : Animal <BeautyContest>

- (NSString *) talkToMe: (NSString *) myName;

- (void) performTrick;

- (void) makeSound;

@end

#endif /* Koala_h */
