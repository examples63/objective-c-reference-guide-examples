//
//  Koala.m
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 08/12/2022.
//

#import <Foundation/Foundation.h>
#import "Koala.h"

@implementation Koala

    // Override method :

- (NSString *) talkToMe: (NSString *) myName {
    
    return [NSString stringWithFormat: @"Hello %@, my name is %@ and I am a Koala.", myName, self.name];
}

    // Add methods :

-(void) performTrick{
    NSLog(@"Koala %@ performs a hand stand", self.name);
}

- (void) makeSound{
    NSLog(@"Koala %@ says Yawn", self.name);
}

@end
