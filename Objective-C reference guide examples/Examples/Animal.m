//
//  Animal.m
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#import <Foundation/Foundation.h>
#import "Animal.h"

@interface Animal ()

// define private properties

// define private methods

@end

// @implementation classname : superclass
@implementation Animal {
    
    // define private instance variables
}


    // Custom Initializer

- (id)initWithName:(NSString*) defaultName {
    if ((self = [super init])) {
        _name = defaultName;
        // self.name = defaultName;
    }
    return self;
}


    // implement methods

// instance method:
// - (return type) name :(type) arg1 :(type) arg2

- (void) getInfo {
    NSLog(@"I am an animal named %@", self.name);
}

// typical use:

- (NSString *) talkToMe: (NSString *) myName {
    
    return [NSString stringWithFormat: @"Hello %@, my name is %@", myName, self.name];
}

-(int) getSum: (int) num1
   nextNumber: (int) num2 {
    
    return num1 + num2;
}

// anonymous arguments:

- (int) anonArgs: (int) a : (int) b : (int) c {
    
    return a + b - c;
}

    // static class method:

// + (return type) name :(type) arg1 :(type) arg2

+ (NSString *)processStrings:(NSString *)first
                      second:(NSString *)second {
    
    return [NSString stringWithFormat: @"%@, %@", first, second];
}

@end
