//
//  Animal.h
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#ifndef Animal_h
#define Animal_h

#import <Foundation/Foundation.h>

@interface Animal : NSObject
// @interface classname (category) <protocols> : superclass {
// @public
// @protected    (default)
// @private
// The default visibility of instance variables is @protected.

/*
    ------------------------
    Define public properties
    ------------------------
 */
@property (strong, nonatomic) NSString *name;
@property NSString *favFood;
@property NSString *sound;

@property float weight;

    // Property Attributes:

// The default attributes are assign and readwrite.

// strong    =  Adds reference to keep object alive
// weak      =  Object can disappear, become nil
// assign      =  Normal assign, no reference (default)
// copy      =  Make copy on assign
// nonatomic =  Make not threadsafe, increase perf
// readwrite =  Create getter&setter (default)
// readonly  =  Create just getter
// getter=name  =  explicitly name getter
// setter=name  =  explicitly name setter

    // Properties automatically create setter and getter methods and private instance variable for a class attribute.

// 1) Private instance variable:
//
// type _propertyName;
//
// 2) Getter and Setter:
//
// - (type)propertyName;
// - (void)setPropertyName:(type)name;
//
// Using _propertyName uses the private instance variable directly. Using self.propertyName uses the Getter/Setter.

/*
    ------------------------
    Define public methods
    ------------------------
 */

    // Constructor

-(instancetype) initWithName: (NSString*) defaultName;


    // instance methods:

// - (return type) name:(type) arg1 arg2:(type) arg2

- (void) getInfo;

// typical use:

- (NSString *) talkToMe: (NSString *) myName;

-(int) getSum: (int) num1
   nextNumber: (int) num2;

// anonymous arguments:

- (int) anonArgs: (int) a : (int) b : (int) c;

    // static class method:

// + (return type) name :(type) arg1 :(type) arg2

+ (NSString *)processStrings:(NSString *)first
                      second:(NSString *)second;

@end

#endif /* Animal_h */
