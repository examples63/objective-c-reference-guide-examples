//
//  SceneDelegate.h
//  Objective-C reference guide examples
//
//  Created by Martin Kompan on 30/11/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

